package com.example.servicefragment

import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() ,TwoFragment.OnFragmentInteractionListener{
    override fun onFragmentInteraction(String: Uri) {
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val twoFragment :TwoFragment =TwoFragment()

        val brankFragment= twoFragment.newInstance("Hello","God")

        val fragmentInstant :FirstFragment =FirstFragment()

        if (savedInstanceState==null){

            supportFragmentManager.beginTransaction()
                  .add(R.id.frameLayout,twoFragment,"Fragment2")
                  .addToBackStack(null)
                  .commit()


        }

        bt_add.setOnClickListener {
            if (!fragmentInstant.isAdded){
                supportFragmentManager.beginTransaction()
                    .add(R.id.frameLayout,fragmentInstant,"Fragment")
                    .commit()
            }
        }
        bt_remove.setOnClickListener {
            supportFragmentManager.beginTransaction()
                .remove(fragmentInstant)
                .commit()
        }
    }
}
